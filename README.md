# darknet-react-interface

To run this project you'll need to clone three repositories next to each other:

Darknet
---
```
#get and build darknet itself
git clone https://github.com/pjreddie/darknet
cd darknet
make
```
If you have CUDA drivers and the library path configured for the CUDA toolkit enable CUDA by opening the makefile and setting the `CUDA=1`

Back end
---
```
#get the npm interface and launch it
git clone https://bitbucket.org/shaun_slalom/darknet-node-interface.git
cd darknet-node-interface
#installs dependencies, alternative npm install
yarn install

# expects the cwd to be darknet so go there first, will fix some of the command code shortly to avoid security issues
cd ../darknet
#starts server on localhost:3001 with CORS enabled, alternative use pm2 for watching index.js for changes, pm2 start index.js --watch
node ../darknet-node-interface/index.js
```
Front end
---
```
#get the react interface and launch it
git clone https://bitbucket.org/shaun_slalom/darknet-react-interface.git
yarn install

# run the fetch all script to get weight/config files for the NN, this will use wget to fetch all the necessary trained models for you
./fetchAllCfgAndWeights.sh

#starts a server on localhost:3000 for serving the compiled front end
npm start
```
You should end up with a folder that has all three projects side by side in terms of folder structure:

  - Top Level Folder
    - darknet
	- darknet-node-interface
	- darknet-react-interface
