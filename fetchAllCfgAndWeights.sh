#!/bin/bash

# AlexNet
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/alexnet.cfg -O ../darknet/cfg/alexnet.cfg
wget http://pjreddie.com/media/files/alexnet.weights -O ../darknet/alexnet.weights

#Darknet Reference
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/darknet.cfg -O ../darknet/cfg/darknet.cfg
wget https://pjreddie.com/media/files/darknet.weights -O ../darknet/darknet.weights

#VGG-16
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/vgg-16.cfg -O ../darknet/cfg/vgg-16.cfg
wget https://pjreddie.com/media/files/vgg-16.weights -O ../darknet/vgg-16.weights

#Extraction
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/extraction.cfg -O ../darknet/cfg/extraction.cfg
wget https://pjreddie.com/media/files/extraction.weights -O ../darknet/extraction.weights

#Darknet19
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/darknet19.cfg -O ../darknet/cfg/darknet19.cfg
wget https://pjreddie.com/media/files/darknet19.weights -O ../darknet/darknet19.weights

#Darknet19 448x448
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/darknet19_448.cfg -O ../darknet/cfg/darknet19_448.cfg
wget https://pjreddie.com/media/files/darknet19_448.weights -O ../darknet/darknet19_448.weights

#Resnet 50
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/resnet50.cfg -O ../darknet/cfg/resnet50.cfg
wget https://pjreddie.com/media/files/resnet50.weights -O ../darknet/resnet50.weights

#Resnet 152
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/resnet152.cfg -O ../darknet/cfg/resnet152.cfg
wget https://pjreddie.com/media/files/resnet152.weights -O ../darknet/resnet152.weights

#Densenet 201
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/densenet201.cfg -O ../darknet/cfg/densenet201.cfg
wget https://pjreddie.com/media/files/densenet201.weights -O ../darknet/densenet201.weights

# Recurrent Neural Network weight files
## George R.R. Martin
wget https://pjreddie.com/media/files/grrm.weights -O ../darknet/grrm.weights

## William Shakespeare
wget https://pjreddie.com/media/files/shakespeare.weights -O ../darknet/shakespeare.weights

## Immanuel Kant
wget https://pjreddie.com/media/files/kant.weights -O ../darknet/kant.weights

## Leo Tolstoy
wget https://pjreddie.com/media/files/tolstoy.weights -O ../darknet/tolstoy.weights