const detectorArgs = [ 'detector', 'test' ];

const classifierArgs = [ 'classifier', 'predict', 'cfg/imagenet1k.data' ];

const nightmareArgs = [ 'nightmare' ];

const rnnArgs = [ 'rnn', 'generate', 'cfg/rnn.cfg' ];

class Command {
	constructor(
		label,
		commandArgs,
		nnType = 'classifier',
		generatesPrediction = false,
		command = 'darknet',
		confidenceThreshold = 20
	) {
		this.id = Command.id++;
		this.label = label;
		this.commandArgs = commandArgs;
		this.command = command;
		this.nnType = nnType;
		this.confidenceThreshold = confidenceThreshold;
	}
	clone() {
		return new Command(this.label, this.commandArgs, this.nnType, this.generatesPrediction, this.command);
	}
}
Command.id = 0;

// This is going to be a very insecure thing but assuming no one is using this
// in the public, going to allow the client to send through the command/arguments
// to run on the server.
const classificationCommands = [
	new Command('AlexNet', [ ...classifierArgs, 'cfg/alexnet.cfg', 'alexnet.weights' ]),
	new Command('Darknet Reference', [ ...classifierArgs, 'cfg/darknet.cfg', 'darknet.weights' ]),
	new Command('VGG-16', [ ...classifierArgs, 'cfg/vgg-16.cfg', 'vgg-16.weights' ]),
	new Command('Extraction', [ ...classifierArgs, 'cfg/extraction.cfg', 'extraction.weights' ]),
	new Command('Darknet19', [ ...classifierArgs, 'cfg/darknet19.cfg', 'darknet19.weights' ]),
	new Command('Darknet19 448x448', [ ...classifierArgs, 'cfg/darknet19_448.cfg', 'darknet19_448.weights' ]),
	new Command('Resnet 50', [ ...classifierArgs, 'cfg/resnet50.cfg', 'resnet50.weights' ]),
	new Command('Resnet 152', [ ...classifierArgs, 'cfg/resnet152.cfg', 'resnet152.weights' ]),
	new Command('Densenet 201', [ ...classifierArgs, 'cfg/densenet201.cfg', 'densenet201.weights' ])
];

const realtimeObjectDetectionCommands = [
	// Commands not working yet
	// new Command('YOLO', [ 'detect', 'cfg/yolo.cfg', 'yolo.weights' ], 'detector', true),
	// new Command('YOLOv2', [ 'detect', 'cfg/yolo-voc.2.0.cfg', 'yolo-voc.weights' ], 'detector', true),
	new Command(
		'YOLOv3',
		['detect', 'cfg/yolov3.cfg', 'yolov3.weights'],
		'detector',
		true
	),
	new Command(
		'YOLO9000',
		[ ...detectorArgs, 'cfg/combine9k.data', 'cfg/yolo9000.cfg', 'yolo9000.weights' ],
		'detector',
		true
	)
];

const nightmareCommands = [
	new Command(
		'VGG-16 (weights and convolution config)',
		[ ...nightmareArgs, 'cfg/vgg-conv.cfg', 'vgg-conv.weights' ],
		'nightmare'
	)
	// new Command(
	// 	'jnet (weights and convolution config)',
	// 	[ ...nightmareArgs, 'cfg/jnet-conv.cfg', 'jnet-conv.weights' ],
	// 	'nightmare'
	// )
];

const rnnCommands = [
	new Command('George RR Martin', [ ...rnnArgs, 'grrm.weights', '-srand', '0' ], 'rnn'),
	new Command('William Shakespeare', [ ...rnnArgs, 'shakespeare.weights', '-srand', '0' ], 'rnn'),
	new Command('Leo Tolsty', [ ...rnnArgs, 'tolstoy.weights', '-srand', '0' ], 'rnn'),
	new Command('Immanuel Kant', [ ...rnnArgs, 'kant.weights', '-srand', '0' ], 'rnn'),
	new Command('Issac Asimov', [ ...rnnArgs, 'asimov.weights', '-srand', '0' ], 'rnn')
];

const allCommands = [
	...classificationCommands,
	...realtimeObjectDetectionCommands,
	...nightmareCommands,
	...rnnCommands
];

const findCommandById = (commandID) => {
	return allCommands.find((value) => {
		return value.id === parseInt(commandID, 10);
	});
};

export {
	classificationCommands,
	rnnCommands,
	nightmareCommands,
	realtimeObjectDetectionCommands,
	findCommandById,
	Command
};
