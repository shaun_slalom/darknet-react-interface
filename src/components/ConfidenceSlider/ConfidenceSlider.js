import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Slider from 'rc-slider';
import { sliderChanged } from '../../redux/actions';

let ConfidenceSlider = ({ showThresholdSlider, confidenceThreshold, sliderChangedCallback }) =>
	showThresholdSlider ? (
		<div>
			Confidence Threshold {confidenceThreshold}%
			<Slider
				min={1}
				max={100}
				step={1}
				defaultValue={confidenceThreshold}
				onAfterChange={sliderChangedCallback}
			/>
		</div>
	) : null;

const mapStateToProps = ({ userInteractionReducer }) => {
	const { showThresholdSlider, confidenceThreshold } = userInteractionReducer;
	return {
		showThresholdSlider,
		confidenceThreshold
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		sliderChangedCallback: (val) => {
			dispatch(sliderChanged(val));
		}
	};
};

ConfidenceSlider.propTypes = {
	showThresholdSlider: PropTypes.bool.isRequired
};

ConfidenceSlider = connect(mapStateToProps, mapDispatchToProps)(ConfidenceSlider);

export default ConfidenceSlider;
