import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { topLevelOptionSelected } from '../../redux/actions';
import {
	rnnCommands,
	classificationCommands,
	realtimeObjectDetectionCommands,
	nightmareCommands,
	findCommandById
} from '../../models/CommandDefinitions';

import './CommandSelection.css';

class CommandSelection extends Component {
	render = () => (
		<div className="command-options">
			<h2>Darknet Options</h2>
			<select
				onChange={(event) => {
					this.props.argumentSelected(event.target.value);
				}}
				value={this.props.value}
			>
				<optgroup label="ImageNet 1K Classification Challenge">
					{classificationCommands.map((anArgument) => {
						return (
							<option key={anArgument.id} value={anArgument.id}>
								{anArgument.label}
							</option>
						);
					})}
				</optgroup>
				<optgroup label="Realtime Object Detection">
					{realtimeObjectDetectionCommands.map((anArgument) => {
						return (
							<option key={anArgument.id} value={anArgument.id}>
								{anArgument.label}
							</option>
						);
					})}
				</optgroup>
				<optgroup label="Nightmare">
					{nightmareCommands.map((anArgument) => {
						return (
							<option key={anArgument.id} value={anArgument.id}>
								{anArgument.label}
							</option>
						);
					})}
				</optgroup>
				<optgroup label="RNN">
					{rnnCommands.map((anArgument) => {
						return (
							<option key={anArgument.id} value={anArgument.id}>
								{anArgument.label}
							</option>
						);
					})}
				</optgroup>
			</select>
		</div>
	);
}

const mapStateToProps = ({ userInteractionReducer }) => {
	return {
		value: userInteractionReducer.selectedValue
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		argumentSelected: (commandId) => dispatch(topLevelOptionSelected(findCommandById(commandId)))
	};
};

CommandSelection.propTypes = {
	value: PropTypes.number.isRequired,
	argumentSelected: PropTypes.func.isRequired
};

CommandSelection = connect(mapStateToProps, mapDispatchToProps)(CommandSelection);

export default CommandSelection;
