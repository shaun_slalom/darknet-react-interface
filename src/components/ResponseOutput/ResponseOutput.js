import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

// Conditionally show response output
let ResponseOutput = ({ processing, processedImage, response }) =>
	!processing && processedImage ? (
		<div>
			<div style={{ maxWidth: '80%', margin: 'auto' }}>
				<pre>{response.data}</pre>
			</div>
			<img alt="Darkent Results" className="processed-image" src={processedImage} />
		</div>
	) : null;

const mapStateToProps = ({ userInteractionReducer }) => {
	const { processing, response } = userInteractionReducer;
	const processedImage = response ? response.processedImage : null;
	return {
		processing: processing,
		processedImage,
		response
	};
};

ResponseOutput.propTypes = {
	processing: PropTypes.bool.isRequired,
	processedImage: PropTypes.string,
	responseData: PropTypes.string
};

ResponseOutput = connect(mapStateToProps)(ResponseOutput);

export default ResponseOutput;
