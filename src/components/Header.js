import React from 'react';

const Header = () => {
	return (
		<header className="App-header">
			<h1 className="App-title">DarkNet Test Interface</h1>
		</header>
	);
};

export default Header;
