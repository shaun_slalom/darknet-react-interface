import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

// Static assets
import logo from '../logo.svg';

let LoadingDisplay = ({ showLoadingDisplay }) => {
	return showLoadingDisplay ? <img src={logo} className="App-logo" alt="logo" /> : null;
};

LoadingDisplay.propTypes = {
	showLoadingDisplay: PropTypes.bool.isRequired
};

const mapStateToProps = ({ userInteractionReducer }) => {
	const { processing } = userInteractionReducer;
	return {
		showLoadingDisplay: processing
	};
};

LoadingDisplay = connect(mapStateToProps)(LoadingDisplay);

export default LoadingDisplay;
