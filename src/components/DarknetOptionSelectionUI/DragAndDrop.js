import React from 'react';
import FileDrop from 'react-file-drop';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { newImageSelected } from '../../redux/actions';

let DragAndDrop = ({ showingDropArea, document, dropHandler }) =>
	showingDropArea ? (
		<div className="content-center">
			<div className="my-uploader">
				Drag an image over this box...
				<FileDrop onDrop={dropHandler}>now drop the file(s) for processing.</FileDrop>
			</div>
		</div>
	) : null;

const mapStateToProps = ({ userInteractionReducer }) => {
	const { showingDropArea } = userInteractionReducer;
	return {
		showingDropArea
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		dropHandler: (files, event) => {
			dispatch(newImageSelected(files[0]));
		}
	};
};

DragAndDrop.propTypes = {
	dropHandler: PropTypes.func
};

DragAndDrop = connect(mapStateToProps, mapDispatchToProps)(DragAndDrop);

export default DragAndDrop;
