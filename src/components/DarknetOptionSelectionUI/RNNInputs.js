import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import { seedTextChanged } from '../../redux/actions';

let RNNInputs = ({ showingRNNInputs, rnnSeed, seedChanged }) =>
	showingRNNInputs ? (
		<div>
			<textarea placeholder="Seed for RNN" onChange={seedChanged} value={rnnSeed} />
		</div>
	) : null;

const mapStateToProps = ({ userInteractionReducer }) => {
	const { seedText, showingRNNInputs } = userInteractionReducer;
	return {
		rnnSeed: seedText,
		showingRNNInputs: showingRNNInputs
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		seedChanged: (event) => {
			dispatch(seedTextChanged(event.target.value));
		}
	};
};

RNNInputs.propTypes = {
	showingRNNInputs: PropTypes.bool.isRequired,
	rnnSeed: PropTypes.string.isRequired,
	seedChanged: PropTypes.func.isRequired
};

RNNInputs = connect(mapStateToProps, mapDispatchToProps)(RNNInputs);

export default RNNInputs;
