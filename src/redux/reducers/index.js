import { combineReducers } from 'redux';

import { userInteractionReducer } from './userInteraction';

const AIExplorerApp = combineReducers({
	userInteractionReducer
});

export default AIExplorerApp;
