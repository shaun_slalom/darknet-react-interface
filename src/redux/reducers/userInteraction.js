import {
	TOP_LEVEL_OPTION_SELECTED,
	SEED_TEXT_CHANGE,
	NEW_IMAGE_SELECTED,
	SLIDER_CHANGED,
	RECEIVED_SERVICE_RESPONSE,
	SENT_SERVICE_REQUEST,
	SERVICE_REQUEST_FAILED
} from '../actions';

export const userInteractionReducer = (
	state = {
		selectedValue: 0,
		seedText: 'Seed text ',
		showLoadingDisplay: false,
		showingRNNInputs: false,
		showThresholdSlider: false,
		confidenceThreshold: 30,
		showingDropArea: true,
		selectedImage: null,
		processing: false
	},
	action
) => {
	const { option, seedText, selectedImage, confidenceThreshold, response } = action;
	switch (action.type) {
		case TOP_LEVEL_OPTION_SELECTED:
			return {
				...state,
				selectedValue: option.id,
				showingDropArea: option.nnType !== 'rnn',
				showThresholdSlider: option.nnType === 'classifier',
				showingRNNInputs: option.nnType === 'rnn'
			};
		case SEED_TEXT_CHANGE:
			return {
				...state,
				seedText
			};
		case NEW_IMAGE_SELECTED:
			return {
				...state,
				selectedImage
			};
		case SLIDER_CHANGED:
			return {
				...state,
				confidenceThreshold
			};
		case RECEIVED_SERVICE_RESPONSE:
			return {
				...state,
				response,
				processing: false
			};
		case SENT_SERVICE_REQUEST:
			return {
				...state,
				response,
				processing: true
			};
		case SERVICE_REQUEST_FAILED:
			return {
				...state,
				response: null,
				processing: false
			};
		default:
			return state;
	}
};
