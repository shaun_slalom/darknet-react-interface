export const TOP_LEVEL_OPTION_SELECTED = 'TOP_LEVEL_OPTION_SELECTED';
export const topLevelOptionSelected = (option) => {
	return {
		type: TOP_LEVEL_OPTION_SELECTED,
		option
	};
};

export const SLIDER_CHANGED = 'SLIDER_CHANGED';
export const sliderChanged = (confidenceThreshold) => {
	return {
		type: SLIDER_CHANGED,
		confidenceThreshold
	};
};

export const SEED_TEXT_CHANGE = 'SEED_TEXT_CHANGE';
export const seedTextChanged = (seedText) => {
	return {
		type: SEED_TEXT_CHANGE,
		seedText
	};
};

export const NEW_IMAGE_SELECTED = 'NEW_IMAGE_SELECTED';
export const newImageSelected = (selectedImage) => {
	return {
		type: NEW_IMAGE_SELECTED,
		selectedImage
	};
};

export const SENT_SERVICE_REQUEST = 'SENT_SERVICE_REQUEST';
export const sendDarknetRequest = (selectedImage, commandDetails) => {
	return {
		isFetching: true,
		type: SENT_SERVICE_REQUEST,
		selectedImage,
		commandDetails
	};
};

export const SERVICE_REQUEST_FAILED = 'SERVICE_REQUEST_FAILED';

export const RECEIVED_SERVICE_RESPONSE = 'RECEIVED_SERVICE_RESPONSE';
export const receiveDarknetResponse = (response, commandDetails) => {
	return {
		type: RECEIVED_SERVICE_RESPONSE,
		isFetching: false,
		response,
		commandDetails
	};
};
