export const getSelectedCommandId = (state) => state.userInteractionReducer.selectedValue;
export const getSelectedImage = (state) => state.userInteractionReducer.selectedImage;
export const getSeedText = (state) => state.userInteractionReducer.seedText;
export const getThreshold = (state) => state.userInteractionReducer.confidenceThreshold;
