import { take, takeLatest, put, call, select, cancel, fork, all } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import { SendDarknetRequest, SendRNNRequest } from '../../services/DarknetService';
import {
  receiveDarknetResponse,
  NEW_IMAGE_SELECTED,
  TOP_LEVEL_OPTION_SELECTED,
  SEED_TEXT_CHANGE,
  SENT_SERVICE_REQUEST,
  SERVICE_REQUEST_FAILED
} from '../actions';
import * as selectors from '../selectors';
import { findCommandById } from '../../models/CommandDefinitions';
import ServiceConfig from '../../services/ServiceConfig';

function* sendPayload(action) {
  try {
    const commandId = yield select(selectors.getSelectedCommandId);
    const selectedImage = yield select(selectors.getSelectedImage);
    const seedText = yield select(selectors.getSeedText);
    const threshold = yield select(selectors.getThreshold);

    if (commandId !== null) {
      const commandDetails = findCommandById(commandId);
      let requestToMake = null;

      if (!commandDetails) return null;

      commandDetails.confidenceThreshold = threshold;

      if (commandDetails.nnType !== 'rnn' && !selectedImage) return null;

      if (commandDetails.nnType === 'rnn') {
        requestToMake = SendRNNRequest;
      } else {
        requestToMake = SendDarknetRequest;
      }

      yield put({ type: SENT_SERVICE_REQUEST });
      const response = yield call(requestToMake, commandDetails, seedText, selectedImage);

      // If the service didn't have enough data to make the call we will just return silently
      if (!response) return;

      const { extension, nnType, nightmareLocation } = response.data;
      const { backendBase, backendPort } = ServiceConfig;
      response.data.processedImage = `${backendBase}:${backendPort}/images/${nnType}/${nightmareLocation}/${extension}?${new Date()}`;
      yield put(receiveDarknetResponse(response.data));
    }
  } catch (e) {
    yield put({ type: SERVICE_REQUEST_FAILED, e });
  }
}

function* handleInput(input) {
  // debounce by 500ms
  yield call(delay, 1000);
  yield call(sendPayload);
}

function* watchInput() {
  let task;
  while (true) {
    const { seedText } = yield take(SEED_TEXT_CHANGE);
    if (task) yield cancel(task);
    task = yield fork(handleInput, seedText);
  }
}

function* root() {
  yield all([
    takeLatest(TOP_LEVEL_OPTION_SELECTED, sendPayload),
    takeLatest(NEW_IMAGE_SELECTED, sendPayload),
    call(watchInput)
  ]);
}

export default root;
