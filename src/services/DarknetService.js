import axios from 'axios';
import ServiceConfig from '../services/ServiceConfig';

const { backendBase, backendPort } = ServiceConfig;

const SendDarknetRequest = (commandDetails, rnnSeed, imageFile) => {
  const data = new FormData();
  if (imageFile) data.append('uploadedFile', imageFile);
  data.append('commandDetails', JSON.stringify(commandDetails));

  return axios.post(`${backendBase}:${backendPort}/upload`, data);
};
const SendRNNRequest = (commandDetails, rnnSeed) => {
  const data = new FormData();
  if (rnnSeed) data.append('rnnSeed', rnnSeed);
  data.append('commandDetails', JSON.stringify(commandDetails));

  return axios.post(`${backendBase}:${backendPort}/rnn`, data);
};

export { SendDarknetRequest, SendRNNRequest };
