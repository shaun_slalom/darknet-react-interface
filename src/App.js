import React, { Component } from 'react';

// Third party component/service/utility libraries
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import mySaga from './redux/sagas/APICallSagas';
// Stylesheets
import './App.css';
import 'rc-slider/assets/index.css';
import './Slider.css';

// App specific

// Reducers
import AIExplorerApp from './redux/reducers';

// Components
import ResponseOutput from './components/ResponseOutput/ResponseOutput';
import ConfidenceSlider from './components/ConfidenceSlider';
import RNNInputs from './components/DarknetOptionSelectionUI/RNNInputs';
import DragAndDrop from './components/DarknetOptionSelectionUI/DragAndDrop';
import LoadingDisplay from './components/LoadingDisplay';
import Header from './components/Header';
import CommandSelection from './components/CommandSelection';

const sagaMiddlware = createSagaMiddleware();
const store = createStore(AIExplorerApp, composeWithDevTools(applyMiddleware(sagaMiddlware)));

sagaMiddlware.run(mySaga);
class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<div className="App">
					<Header />
					<CommandSelection />
					<ConfidenceSlider />
					<DragAndDrop />
					<RNNInputs />
					<LoadingDisplay />
					<ResponseOutput />
				</div>
			</Provider>
		);
	}
}

export default App;
